import java.util.stream.IntStream;

public class Challenge {
	public static int countTrue(boolean[] arr) {
		int coun = 0;
		for (boolean i :arr) if (i) coun += 1;
		return coun;
	}
}
